.. _tutorials:

Tutorials
=========

Config of eDFTpy script
-----------------------

.. toctree::
   :maxdepth: 1

   config

DFTpy
-----

.. toctree::
   :maxdepth: 1

   ofdft/dftpy/dftpy

QUANTUM-ESPRESSO
----------------

.. toctree::
   :maxdepth: 2

   ksdft/qe/qe

sDFT using eDFTpy
-----------------

.. toctree::
   :maxdepth: 2

   sdft/optimize/optimize
   sdft/optimize-2/optimize-2
   sdft/relax/relax
   sdft/md/md

